<?php
// HTTP
define('HTTP_SERVER', 'http://localhost:8888/nampak7/admin/');
define('HTTP_CATALOG', 'http://localhost:8888/nampak7/');

// HTTPS
define('HTTPS_SERVER', 'https://localhost:8888/nampak7/admin/');
define('HTTPS_CATALOG', 'https://localhost:8888/nampak7/');

// DIR
define('DIR_APPLICATION', '/vagrant/src/nampak7/admin/');
define('DIR_SYSTEM', '/vagrant/src/nampak7/system/');
define('DIR_LANGUAGE', '/vagrant/src/nampak7/admin/language/');
define('DIR_TEMPLATE', '/vagrant/src/nampak7/admin/view/template/');
define('DIR_CONFIG', '/vagrant/src/nampak7/system/config/');
define('DIR_IMAGE', '/vagrant/src/nampak7/image/');
define('DIR_CACHE', '/vagrant/src/nampak7/system/cache/');
define('DIR_DOWNLOAD', '/vagrant/src/nampak7/system/download/');
define('DIR_UPLOAD', '/vagrant/src/nampak7/system/upload/');
define('DIR_LOGS', '/vagrant/src/nampak7/system/logs/');
define('DIR_MODIFICATION', '/vagrant/src/nampak7/system/modification/');
define('DIR_CATALOG', '/vagrant/src/nampak7/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'nampak7');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
