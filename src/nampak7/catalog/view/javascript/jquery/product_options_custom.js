$(function(){
    var sphereLef = ["2.00", "3.00", "4.00", "5.00"];
    $('input[type=radio][name="option[546]"]').change(function() {
        $("#input-option547 > option").each(function() {
            if ($.inArray(this.text, sphereLef) == -1) {
                $(this).attr("disabled", true);
            }
        });
    });
});